""" Find all people on Purkyne dormitories """
import time
from datetime import datetime
import requests
from bs4 import BeautifulSoup

date_time = datetime.now()
date = date_time.date()
date = date.strftime("%y-%m-%d")

URL = 'https://www.kn.vutbr.cz/is2/index.html'

def rooms_b02():
    """ B02 """
    with open(f"B02_rooms_{date}.txt", "w", encoding="utf-8") as file:
        start_time = time.time()
        for floor in range(1,7):
            for room in range(1, 41):
                if room < 10:
                    room_number = 'B02-' + str(floor) + '0' + str(room)
                    myobj = {'str': room_number}
                else:
                    room_number = 'B02-' + str(floor) + str(room)
                    myobj = {'str': room_number}

                return_page = requests.post(URL, data = myobj)
                return_page = return_page.text
                soup = BeautifulSoup(return_page, 'lxml')
                file.write(room_number + '\n')
                if soup.find_all('th'):
                    for name in soup.find_all('th'):
                        file.write('   ' + name.text + '\n')
                print(room_number)
                time.sleep(0.5)

        print("--- %s seconds ---" % (time.time() - start_time))
        file.close()

def rooms_b04():
    """ B04 """
    with open(f"B04_rooms_{date}.txt", "w", encoding="utf-8") as file:
        start_time = time.time()
        for floor in range(1,14):
            for room in range(1, 23):
                if room < 10:
                    room_number = 'B04-' + str(floor) + '0' + str(room)
                    myobj = {'str': room_number}
                else:
                    room_number = 'B04-' + str(floor) + str(room)
                    myobj = {'str': room_number}

                return_page = requests.post(URL, data = myobj)
                return_page = return_page.text
                soup = BeautifulSoup(return_page, 'lxml')
                file.write(room_number + '\n')
                if soup.find_all('th'):
                    for name in soup.find_all('th'):
                        file.write('   ' + name.text + '\n')

                print(room_number)
                time.sleep(0.5)

        print("--- %s seconds ---" % (time.time() - start_time))
        file.close()

def rooms_b05():
    """ B05 """
    with open(f"B05_rooms_{date}.txt", "w", encoding="utf-8") as file:
        start_time = time.time()
        for floor in range(1,7):
            for room in range(1, 39):
                if room < 10:
                    room_number = 'B05-' + str(floor) + '0' + str(room)
                    myobj = {'str': room_number}
                else:
                    room_number = 'B05-' + str(floor) + str(room)
                    myobj = {'str': room_number}

                return_page = requests.post(URL, data = myobj)
                return_page = return_page.text
                soup = BeautifulSoup(return_page, 'lxml')
                file.write(room_number + '\n')
                if soup.find_all('th'):
                    for name in soup.find_all('th'):
                        file.write('   ' + name.text + '\n')

                print(room_number)
                time.sleep(0.5)

        print("--- %s seconds ---" % (time.time() - start_time))
        file.close()

def rooms_b07():
    """ B07 """
    with open(f"B07_rooms_{date}.txt", "w", encoding="utf-8") as file:
        start_time = time.time()
        for floor in range(1,14):
            for room in range(1, 23):
                if room < 10:
                    room_number = 'B07-' + str(floor) + '0' + str(room)
                    myobj = {'str': room_number}
                else:
                    room_number = 'B07-' + str(floor) + str(room)
                    myobj = {'str': room_number}

                return_page = requests.post(URL, data = myobj)
                return_page = return_page.text
                soup = BeautifulSoup(return_page, 'lxml')
                file.write(room_number + '\n')
                if soup.find_all('th'):
                    for name in soup.find_all('th'):
                        file.write('   ' + name.text + '\n')

                print(room_number)
                time.sleep(0.5)

        print("--- %s seconds ---" % (time.time() - start_time))
        file.close()

def main():
    """ main """
    rooms_b02()
    rooms_b04()
    rooms_b05()
    rooms_b07()

if __name__ == "__main__":
    main()
